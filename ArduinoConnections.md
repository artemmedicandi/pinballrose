**2x Bank Drop Targets**
- 6 Inputs
- (5V)
- 2 Outputs Reset-Solenoid

**7x Rollover Switches**
- (5V)
- 7 Inputs

**3x Pop Bumper**
- 3 Outputs LEDs
- 3 Outputs Activation
- 3 Inputs Activation

**4x Standing Targets**
- (5V)
- 4 Inputs

**2x Slingshots**
- 2 Inputs
- 2 Outputs

**Kickback**
- 1 Output

**BallReturn (Essentially a Kickback...)**
- 1 Output

**BallReturnSwitch**
- (5V)
- 1 Input


**19x LEDs**
_Multiplexing_
- 9 Outputs


**Total**
| What    | Amount |
| ------- | ------ |
| Outputs | 21     |
| Inputs  | 23     |
| Tutto   | 44     |




 


# Outputs
## LEDs

* 3 Pop Bumper LEDs
* 19 Normal LEDs






# Inputs
## 







