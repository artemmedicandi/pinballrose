/*

    Creation Date:  15. Dec 2020
    Authors:        Lukas D., Elias G.

    Main file to control the parts on the pinball playfield.

*/

/*
 * Dynamic exception specifications are deprecated since C++11 (and illegal since C++17).
 * Because of that, the compiler will give a lot of warnings.
 * Still, we will use the Libraries for the C++ std functions.
 *
 * This pragma will disable all the warnings on the compilation output:
 */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated"
#include <Arduino.h>
#include <ArduinoSTL.h>

#include <vector>
#pragma GCC diagnostic pop

#pragma region Definitions
#define LEDFreq 1  // 1000ms in 1s, 1000 / 20 == 50Hz

#define PHASE_0 0
#define PHASE_1 1
#define PHASE_2 2
#define PHASE_2 2

// Bank Drop Targets
#define BDT_IN_0_0 22
#define BDT_IN_0_1 23
#define BDT_IN_0_2 24
#define BDT_IN_1_0 25
#define BDT_IN_1_1 26
#define BDT_IN_1_2 27
// BDT Reset switches
#define BDT_OUT_0 28
#define BDT_OUT_1 29

// RollOver Switches
#define ROS_START 30
#define ROS_MULT 31
#define ROS_TOP_0 32
#define ROS_TOP_1 33
#define ROS_TOP_2 34
#define ROS_TOP_3 35
#define ROS_KB 36

// Pop Bumper
#define PB_0_LED 37
#define PB_0_IN 43
#define PB_0_OUT 38

#define PB_1_LED 39
#define PB_1_IN 44
#define PB_1_OUT 40

#define PB_2_LED 41
#define PB_2_IN 45
#define PB_2_OUT 42

// Standing Targets
#define ST_0 46
#define ST_1 47
#define ST_2 48
#define ST_3 49

// Slingshots
#define SS_L_IN 50
#define SS_L_OUT 7
#define SS_R_IN 51
#define SS_R_OUT 53

// Spinners
#define SPIN_0 9
#define SPIN_1 10

// KickBack
#define KB 11

// BallReturn
#define BR 12

// BallReturnSwitch
#define BRS 13

// Scoop Assembly
#define SCOOP_IN 3
#define SCOOP_OUT 2

#pragma endregion Definitions

// int PhaseTime;
int PhaseState;
int startTime;
unsigned long gameCounterTime;
unsigned long gameCounterSeconds;

bool ROS_Start_Activated = false;

long amountHitReference = 1;

// int LEDTime;
// int LEDState;

std::vector<int> xmatrix = {A0, A1, A2, A3, A4};
std::vector<int> ymatrix = {A5, A6, A7, A8};

unsigned long long Score;
unsigned long long ScoreSent;
double Multiplier = 1.05;

bool gameHasStarted;

// int curLEDState;

// std::vector<int> xPinArr{0, 1, 2, 3, 4};
// std::vector<int> yPinArr{5, 6, 7, 8};

#pragma region Classes

class LED {
   public:
    int x;
    int y;

    bool nomatrix = false;
    int PIN;

    static std::vector<LED> AllLEDs;
    static std::vector<LED> runningLEDs;
    static long LEDTime;
    static int LEDState;

    LED(){};
    LED(int _x, int _y) {
        this->x = _x;
        this->y = _y;
    };
    LED(int _PIN) {
        this->PIN = _PIN;
        this->nomatrix = true;
    };
    static void ResetLEDs(bool PBs) {
        for (LED led : LED::AllLEDs) {
            if (led.nomatrix) {
                continue;
            }
            digitalWrite(led.x, HIGH);
            digitalWrite(led.y, LOW);
        }
        if (PBs) {
            digitalWrite(PB_0_LED, LOW);
            digitalWrite(PB_1_LED, LOW);
            digitalWrite(PB_2_LED, LOW);
        }
        // delay(500);
    }
    static void Attract() {
        for (LED led : LED::AllLEDs) {
            if (led.nomatrix) {
                continue;
            }
            digitalWrite(led.x, LOW);
            digitalWrite(led.y, HIGH);
        }
        digitalWrite(PB_0_LED, LOW);
        digitalWrite(PB_1_LED, LOW);
        digitalWrite(PB_2_LED, LOW);
    }
    static void BlinkRandomLEDs() {
        if (millis() - LED::LEDTime < 150) {
            return;
        }

        LED::ResetLEDs(true);

        int rand = random(0, ((LED::AllLEDs).size() - 1));
        LED randomLED = LED::AllLEDs[rand];
        // randomLED = LED::AllLEDs[rand];
        if (randomLED.nomatrix) {
            digitalWrite(randomLED.PIN, HIGH);
        } else {
            digitalWrite(randomLED.x, LOW);
            digitalWrite(randomLED.y, HIGH);
        }
        LED::LEDTime = millis();
        // randomLED.~LED();
    }
    static void LightLEDs() {
        if ((millis() - LED::LEDTime < LEDFreq)) {
            return;
        }

        LED::ResetLEDs(true);

        LED currentLED = LED::runningLEDs[((int)LED::LEDState)];
        // currentLED = LED::runningLEDs[((int)LED::LEDState)];
        digitalWrite(currentLED.x, LOW);
        digitalWrite(currentLED.y, HIGH);
        // currentLED.~LED();
        LED::LEDState = LED::LEDState + 1;
        if (LED::LEDState >= (LED::runningLEDs).size()) {
            LED::LEDState = 0;
        }

        LED::LEDTime = millis();
    }
    static void AddLED(LED led) {
        for (LED l : LED::runningLEDs) {
            if (led.x == l.x && led.y == l.y) {
                return;
            }
        }

        LED::runningLEDs.push_back(led);
    }
    /*
    static void RemoveLED(LED led) {
        std::vector<LED> temprunningLEDs;
        for (int i = 0; i <= (LED::runningLEDs).size(); i++) {
            if (LED::runningLEDs[i].x == led.x && LED::runningLEDs[i].y == led.y) {
                temprunningLEDs.push_back(LED::runningLEDs[i]);
            }
        }
        LED::runningLEDs = temprunningLEDs;
        temprunningLEDs.vector::~vector();
    }
    */
};
class Target {
   public:
    int amountHit;
    LED led;
    int PIN;
    bool counted = false;
    unsigned long timeHit;
    Target(){};
    Target(LED _led, int _PIN) {
        this->led = _led;
        this->PIN = _PIN;
    };

    void CheckTarget() {
        if (this->counted) {
            return;
        }
        if (digitalRead(this->PIN) == HIGH) {
            LED::AddLED(this->led);
            this->amountHit++;
            Score += 30;
            this->counted = true;
            gameHasStarted = true;
        }
    }
};
class BankDropTarget {
   public:
    Target T0;
    Target T1;
    Target T2;
    int amountHit;
    int ResetPIN;
    BankDropTarget(){};
    BankDropTarget(Target _T0, Target _T1, Target _T2, int _ResetPIN) {
        this->T0 = _T0;
        this->T1 = _T1;
        this->T2 = _T2;
        this->ResetPIN = _ResetPIN;
    };
    void CheckBDT() {
        if (this->T0.counted && this->T1.counted && this->T2.counted) {
            this->amountHit++;
            Score += 110;
            this->ResetBDT();

            this->T0.counted = false;
            this->T1.counted = false;
            this->T2.counted = false;
        }
    }

   private:
    void ResetBDT() {
        digitalWrite(this->ResetPIN, HIGH);
        delay(500);
        digitalWrite(this->ResetPIN, LOW);
    }
};
class Switch {
   public:
    bool Hit;
    int amountHit;
    LED led;
    int PIN;
    unsigned long timeHit;
    Switch(){};
    Switch(int _PIN) { this->PIN = _PIN; };
    Switch(LED _led, int _PIN) {
        this->led = _led;
        this->PIN = _PIN;
    };
    void CheckSwitch() {
        if (millis() - this->timeHit < 500) {
            return;
        }
        if (digitalRead(this->PIN) == LOW) {
            LED::AddLED(this->led);
            this->amountHit++;
            this->Hit = true;
            Score += 100;
            gameHasStarted = true;
            this->timeHit = millis();
        }
    }
    void MultiplierSW() {
        if (millis() - this->timeHit < 750) {
            return;
        }
        if (digitalRead(this->PIN) == LOW) {
            Score *= Multiplier;
            this->timeHit = millis();
        }
    }
    static void CheckSwitchGroup() {
        if (ROSTop0->Hit && ROSTop1->Hit && ROSTop2->Hit && ROSTop3->Hit) {
            Score += 600;

            ROSTop0->Hit = false;
            ROSTop1->Hit = false;
            ROSTop2->Hit = false;
            ROSTop3->Hit = false;
        }
    }
};

class BallReturnSwitch : public Switch {
   public:
    using Switch::Switch;
    // void CheckBRSwitch() {
    //     if (digitalRead(this->PIN) == HIGH) {
    //         PhaseState = PHASE_0;
    //         delay(2000);
    //         digitalWrite(Br->PIN, HIGH);
    //         delay(100);
    //         digitalWrite(Br->PIN, LOW);
    //     }
    // }
};

class PopBumper {
   public:
    LED led;
    int INPIN;
    int OUTPIN;
    int amountHit;  // Not for a group, just as; a gimmick
    unsigned long timeHit;

    PopBumper(){};
    PopBumper(LED _led, int _INPIN, int _OUTPIN) {
        this->led = _led;
        this->INPIN = _INPIN;
        this->OUTPIN = _OUTPIN;
    };
    void CheckPopBumper() {
        if (millis() - this->timeHit < 200) {
            return;
        }
        if (digitalRead(this->INPIN) == LOW) {
            this->FirePopBumper();
            this->amountHit++;

            Score += 5;
            gameHasStarted = true;
            this->timeHit = millis();
        } else {
            this->StopPopBumper();
        }
    }

   private:
    void FirePopBumper() {
        digitalWrite(this->OUTPIN, HIGH);
        digitalWrite(this->led.PIN, HIGH);
    }
    void StopPopBumper() {
        digitalWrite(this->OUTPIN, LOW);
        digitalWrite(this->led.PIN, LOW);
    }
};

class StandingTarget : public Target {
   public:
    using Target::Target;
    bool Hit;
    void CheckTarget() {
        if (millis() - this->timeHit < 500) {
            return;
        }
        if (digitalRead(this->PIN) == LOW) {
            LED::AddLED(this->led);
            this->amountHit++;
            this->Hit = true;
            Score += 50;
            gameHasStarted = true;
            this->timeHit = millis();
        }
    }
    static void CheckTargetGroup() {
        if (ST0->Hit && ST1->Hit && ST2->Hit && ST3->Hit) {
            Score += 200;

            ST0->counted = false;
            ST1->counted = false;
            ST2->counted = false;
            ST3->counted = false;
        }
    }
};

class Slingshot {
   public:
    int amountHit;  // Not for a group, just as a gimmick
    int INPIN;
    int OUTPIN;
    unsigned long timeHit;
    Slingshot(){};
    Slingshot(int _INPIN, int _OUTPIN) {
        this->INPIN = _INPIN;
        this->OUTPIN = _OUTPIN;
    };
    void CheckSlingShot() {
        // if (millis() - this->timeHit < 200) {
        // return;
        //}
        if (digitalRead(this->INPIN) == LOW) {
            this->FireSlingShot();
            this->amountHit++;
            Score += 5;
            this->timeHit = millis();
        } else {
            this->StopSlingShot();
        }
    }

   private:
    void FireSlingShot() {
        digitalWrite(this->OUTPIN, HIGH);
    }
    void StopSlingShot() {
        digitalWrite(this->OUTPIN, LOW);
    }
};

class ScoopAssembly {
    // Infos how to wire SWA: https://docs.missionpinball.org/en/latest/mechs/switches/optos.html
    static const int delaytime = 20;  // 20 ms

   public:
    bool Hit;
    int INPIN;
    int OUTPIN;
    LED led;
    int amountHit;
    ScoopAssembly(){};
    ScoopAssembly(LED _led, int _INPIN, int _OUTPIN) {
        this->led = _led;
        this->INPIN = _INPIN;
        this->OUTPIN = _OUTPIN;
    };
    void CheckScoop() {
        if (digitalRead(this->INPIN) == HIGH) {
            delay(1500);
            digitalWrite(this->OUTPIN, HIGH);
            delay(delaytime);
            digitalWrite(this->OUTPIN, LOW);
            delay(300);
            LED::AddLED(this->led);
            this->amountHit++;
            this->Hit = true;
            Score += 50;
            gameHasStarted = true;
        }
    }
    static void CheckScoopGroup() {
        if (SCOOP->Hit && Kb->Hit) {
            Score += 200;

            SCOOP->Hit = false;
            Kb->Hit = false;
        }
    }
};

class Spinner {
   public:
    bool Hit;
    int amountHit;
    LED led;
    int PIN;
    unsigned long timeHit;
    Spinner(LED _led, int _PIN) {
        this->led = _led;
        this->PIN = _PIN;
    };
    void CheckSpinner() {
        if (millis() - this->timeHit < 500) {
            return;
        }
        if (digitalRead(this->PIN) == LOW) {
            LED::AddLED(this->led);
            this->amountHit++;
            this->Hit = true;
            Score += 40;
            gameHasStarted = true;
            this->timeHit = millis();
        }
    }
    static void CheckSpinnerGroup() {
        if (SPIN_TOP->Hit && SPIN_BOT->Hit) {
            Score += 220;

            SPIN_TOP->Hit = false;
            SPIN_TOP->Hit = false;
        }
    }
};

class Kickback {
   public:
    int amountHit;
    Switch sw;
    int PIN;
    unsigned long timeHit;
    bool Hit;
    Kickback(){};
    Kickback(Switch _sw, int _PIN) {
        this->PIN = _PIN;
        this->sw = _sw;
    };

    void CheckKickback() {
        if (millis() - this->timeHit > 1750 && millis() - this->timeHit < 2500) {
            return;
        }
        if (digitalRead(this->sw.PIN) == LOW) {
            this->timeHit = millis();
            this->Hit = true;
        }
        if (this->Hit) {
            if (millis() - this->timeHit < 1500) {
                return;
            } else if (millis() - this->timeHit < 1750) {
                digitalWrite(this->PIN, HIGH);

            } else {
                digitalWrite(this->PIN, LOW);
                LED::AddLED(this->sw.led);
                this->sw.amountHit++;
                Score += 50;
                gameHasStarted = true;
                this->Hit = false;
            }
        }
    }
};

class BallReturn {
   public:
    BallReturnSwitch sw;
    int PIN;
    LED led;
    BallReturn(){};
    BallReturn(LED _led, BallReturnSwitch _sw, int _PIN) {
        this->led = _led;
        this->PIN = _PIN;
        this->sw = _sw;
    };
    void CheckBR() {
        if (digitalRead(this->sw.PIN) == LOW) {
            PhaseState = PHASE_0;
            Serial.println("GAME_ENDED");
            LED::runningLEDs.clear();
            LED::AddLED(this->led);
            LED::LightLEDs();
            delay(3000);
            digitalWrite(this->PIN, HIGH);
            delay(100);
            digitalWrite(this->PIN, LOW);
            ROS_Start_Activated = false;
            Score = 0;
            Multiplier = 1.05;
            LED::runningLEDs.clear();
        }
    }
};

#pragma endregion Classes

std::vector<LED> LED::AllLEDs{};
std::vector<LED> LED::runningLEDs{};
long LED::LEDTime = millis();
int LED::LEDState = 0;

BankDropTarget* BDT1;
BankDropTarget* BDT2;

Switch* ROSStart;
Switch* ROSMult;
Switch* ROSTop0;
Switch* ROSTop1;
Switch* ROSTop2;
Switch* ROSTop3;
Switch* ROSKickBack;

PopBumper* PopB0;
PopBumper* PopB1;
PopBumper* PopB2;

StandingTarget* ST0;
StandingTarget* ST1;
StandingTarget* ST2;
StandingTarget* ST3;

Slingshot* SS_L;
Slingshot* SS_R;

ScoopAssembly* SCOOP;

Spinner* SPIN_TOP;
Spinner* SPIN_BOT;

Kickback* Kb;

BallReturnSwitch* Brs;
BallReturn* Br;

LED* LED0;
LED* LED1;
LED* LED2;
LED* LED3;
LED* LED4;
LED* LED5;
LED* LED6;
LED* LED7;
LED* LED8;
LED* LED9;
LED* LED10;
LED* LED11;
LED* LED12;
LED* LED13;
LED* LED14;
LED* LED15;
LED* LED16;
LED* LED17;
LED* LED18;
// LED* LED19

LED currentLED;
LED randomLED;

void setup() {
#pragma region SetpinModes

    // Matrix

    for (int pin : xmatrix) {
        pinMode(pin, OUTPUT);
    }
    for (int pin : ymatrix) {
        pinMode(pin, OUTPUT);
    }

    // End Matrix

    // Bank Drop Targets
    for (int i = 22; i <= 27; i++) {
        pinMode(i, INPUT);
        // digitalWrite(i, LOW);
    }
    pinMode(BDT_OUT_0, OUTPUT);
    pinMode(BDT_OUT_1, OUTPUT);
    digitalWrite(BDT_OUT_0, LOW);
    digitalWrite(BDT_OUT_1, LOW);
    // End Bank Drop Targets

    // Rollover Switches
    for (int i = 30; i <= 36; i++) {
        pinMode(i, INPUT_PULLUP);
        // digitalWrite(i, LOW);
    }
    // End Rollover Switches

    // Pop Bumper
    for (int i = 37; i <= 42; i++) {
        pinMode(i, OUTPUT);
        digitalWrite(i, LOW);
    }
    pinMode(PB_0_IN, INPUT_PULLUP);
    pinMode(PB_1_IN, INPUT_PULLUP);
    pinMode(PB_2_IN, INPUT_PULLUP);
    // digitalWrite(43, LOW);
    // digitalWrite(44, LOW);
    // digitalWrite(45, LOW);

    // End Pop Bumper

    // Standing Targets
    for (int i = 46; i <= 49; i++) {
        pinMode(i, INPUT_PULLUP);
        // digitalWrite(i, LOW);
    }
    // End Standing Targets

    // Slingshots
    pinMode(SS_L_IN, INPUT_PULLUP);
    pinMode(SS_R_IN, INPUT_PULLUP);
    pinMode(SS_L_OUT, OUTPUT);
    pinMode(SS_R_OUT, OUTPUT);

    // digitalWrite(50, LOW);
    // digitalWrite(51, LOW);
    digitalWrite(SS_L_OUT, LOW);
    digitalWrite(SS_R_OUT, LOW);
    // End Slingshots

    // Kickback
    pinMode(KB, OUTPUT);
    digitalWrite(KB, LOW);
    // BallReturn
    pinMode(BR, OUTPUT);
    digitalWrite(BR, LOW);
    // BallReturnSwitch
    pinMode(BRS, INPUT_PULLUP);
    // digitalWrite(BRS, LOW);

    // Scoop Weldment Assembly
    pinMode(SCOOP_IN, INPUT_PULLUP);
    pinMode(SCOOP_OUT, OUTPUT);
    digitalWrite(SCOOP_OUT, LOW);
    // End Scoop Weldment Assembly

    // Spinners
    pinMode(SPIN_0, INPUT_PULLUP);
    pinMode(SPIN_1, INPUT_PULLUP);
    // End Spinners

#pragma endregion SetpinModes

    // Initialise LEDs
    LED0 = new LED(xmatrix[0], ymatrix[0]);
    LED1 = new LED(xmatrix[0], ymatrix[1]);
    LED2 = new LED(xmatrix[0], ymatrix[2]);
    LED3 = new LED(xmatrix[0], ymatrix[3]);
    LED4 = new LED(xmatrix[1], ymatrix[0]);
    LED5 = new LED(xmatrix[1], ymatrix[1]);
    LED6 = new LED(xmatrix[1], ymatrix[2]);
    LED7 = new LED(xmatrix[1], ymatrix[3]);
    LED8 = new LED(xmatrix[2], ymatrix[0]);
    LED9 = new LED(xmatrix[2], ymatrix[1]);
    LED10 = new LED(xmatrix[2], ymatrix[2]);
    LED11 = new LED(xmatrix[2], ymatrix[3]);
    LED12 = new LED(xmatrix[3], ymatrix[0]);
    LED13 = new LED(xmatrix[3], ymatrix[1]);
    LED14 = new LED(xmatrix[3], ymatrix[2]);
    LED15 = new LED(xmatrix[3], ymatrix[3]);
    LED16 = new LED(xmatrix[4], ymatrix[0]);
    LED17 = new LED(xmatrix[4], ymatrix[1]);
    LED18 = new LED(xmatrix[4], ymatrix[2]);
    // LED19 = new LED(xmatrix[4], ymatrix[3]);

    LED::AllLEDs.push_back(*LED0);
    LED::AllLEDs.push_back(*LED1);
    LED::AllLEDs.push_back(*LED2);
    LED::AllLEDs.push_back(*LED3);
    LED::AllLEDs.push_back(*LED4);
    LED::AllLEDs.push_back(*LED5);
    LED::AllLEDs.push_back(*LED6);
    LED::AllLEDs.push_back(*LED7);
    LED::AllLEDs.push_back(*LED8);
    LED::AllLEDs.push_back(*LED9);
    LED::AllLEDs.push_back(*LED10);
    LED::AllLEDs.push_back(*LED11);
    LED::AllLEDs.push_back(*LED12);
    LED::AllLEDs.push_back(*LED13);
    LED::AllLEDs.push_back(*LED14);
    LED::AllLEDs.push_back(*LED15);
    LED::AllLEDs.push_back(*LED16);
    LED::AllLEDs.push_back(*LED17);
    LED::AllLEDs.push_back(*LED18);

    LED* LEDPB0 = new LED(PB_0_LED);
    LED* LEDPB1 = new LED(PB_1_LED);
    LED* LEDPB2 = new LED(PB_2_LED);

    LED::AllLEDs.push_back(*LEDPB0);
    LED::AllLEDs.push_back(*LEDPB1);
    LED::AllLEDs.push_back(*LEDPB2);

    // Create Target Objects
    // Bank Drop Targets
    BDT1 = new BankDropTarget(
        *(new Target(*LED1, BDT_IN_0_0)),
        *(new Target(*LED5, BDT_IN_0_1)),
        *(new Target(*LED9, BDT_IN_0_2)), BDT_OUT_0);
    BDT2 = new BankDropTarget(
        *(new Target(*LED3, BDT_IN_1_0)),
        *(new Target(*LED7, BDT_IN_1_1)),
        *(new Target(*LED11, BDT_IN_1_2)), BDT_OUT_1);

    ROSStart = new Switch(*LED6, ROS_START);
    ROSTop0 = new Switch(*LED0, ROS_TOP_0);
    ROSTop1 = new Switch(*LED4, ROS_TOP_1);
    ROSTop2 = new Switch(*LED8, ROS_TOP_2);
    ROSTop3 = new Switch(*LED12, ROS_TOP_3);
    ROSMult = new Switch(ROS_MULT);
    ROSKickBack = new Switch(*LED18, ROS_KB);

    PopB0 = new PopBumper(*LEDPB0, PB_0_IN, PB_0_OUT);
    PopB1 = new PopBumper(*LEDPB1, PB_1_IN, PB_1_OUT);
    PopB2 = new PopBumper(*LEDPB2, PB_2_IN, PB_2_OUT);

    ST0 = new StandingTarget(*LED2, ST_0);
    ST1 = new StandingTarget(*LED6, ST_1);
    ST2 = new StandingTarget(*LED10, ST_2);
    ST3 = new StandingTarget(*LED14, ST_3);

    SS_L = new Slingshot(SS_L_IN, SS_L_OUT);
    SS_R = new Slingshot(SS_R_IN, SS_R_OUT);

    SCOOP = new ScoopAssembly(*LED15, SCOOP_IN, SCOOP_OUT);

    SPIN_TOP = new Spinner(*LED13, SPIN_0);
    SPIN_BOT = new Spinner(*LED17, SPIN_1);

    Kb = new Kickback(*ROSKickBack, KB);

    Brs = new BallReturnSwitch(BRS);
    Br = new BallReturn(*LED16, *Brs, BR);

    // Set up Serial connection to communicate with Raspberry PI
    Serial.begin(9600);

    // Initialise first Phase
    PhaseState = PHASE_0;
    // LED();
    LED::LEDTime = millis();
    LED::LEDState = 0;
}

/*
int ControlLEDs(int LEDState = 30,
    std::vector<int> xarr = {-1},
    std::vector<int> yarr = {-1}) {
    if ((LEDTime - millis() < LEDFreq)) {
        return LEDState;
    }
    if (xarr.size() != yarr.size()) {
        return 0;
    }

    for (int item : xPinArr) {
        if (xarr[LEDState] == item) {
            digitalWrite(item, LOW);
        } else {
            digitalWrite(item, HIGH);
        }
    }
    for (int item : yPinArr) {
        if (xarr[LEDState] == item) {
            digitalWrite(item, HIGH);
        } else {
            digitalWrite(item, LOW);
        }
    }
}
*/

void CheckAllTargets() {
    BDT1->T0.CheckTarget();
    BDT1->T1.CheckTarget();
    BDT1->T2.CheckTarget();
    BDT1->CheckBDT();

    BDT2->T0.CheckTarget();
    BDT2->T1.CheckTarget();
    BDT2->T2.CheckTarget();
    BDT2->CheckBDT();

    ST0->CheckTarget();
    ST1->CheckTarget();
    ST2->CheckTarget();
    ST3->CheckTarget();
    StandingTarget::CheckTargetGroup();

    PopB0->CheckPopBumper();
    PopB1->CheckPopBumper();
    PopB2->CheckPopBumper();
    // No Group

    SS_L->CheckSlingShot();
    SS_R->CheckSlingShot();
    // No Group

    // Switches
    Br->CheckBR();

    ROSTop0->CheckSwitch();
    ROSTop1->CheckSwitch();
    ROSTop2->CheckSwitch();
    ROSTop3->CheckSwitch();
    Switch::CheckSwitchGroup();
    // ROSMult->MultiplierSW();

    SCOOP->CheckScoop();
    Kb->CheckKickback();
    ScoopAssembly::CheckScoopGroup();

    SPIN_TOP->CheckSpinner();
    SPIN_BOT->CheckSpinner();
    Spinner::CheckSpinnerGroup();

    if (digitalRead(ROSMult->PIN) == LOW) {
        if (millis() - ROSMult->timeHit < 750) {
            return;
        }
        if (BDT1->amountHit >= amountHitReference &&
            BDT2->amountHit >= amountHitReference &&

            ST0->amountHit >= amountHitReference &&
            ST1->amountHit >= amountHitReference &&
            ST2->amountHit >= amountHitReference &&
            ST3->amountHit >= amountHitReference &&

            PopB0->amountHit >= amountHitReference &&
            PopB1->amountHit >= amountHitReference &&
            PopB2->amountHit >= amountHitReference &&

            SS_L->amountHit >= amountHitReference &&
            SS_R->amountHit >= amountHitReference &&

            ROSTop0->amountHit >= amountHitReference &&
            ROSTop1->amountHit >= amountHitReference &&
            ROSTop2->amountHit >= amountHitReference &&
            ROSTop3->amountHit >= amountHitReference &&

            SCOOP->amountHit >= amountHitReference &&

            Kb->amountHit >= amountHitReference &&

            SPIN_TOP->amountHit >= amountHitReference &&
            SPIN_BOT->amountHit >= amountHitReference) {
            amountHitReference++;
            Multiplier += 0.05;
        }
        Score *= Multiplier;
        ROSMult->timeHit = millis();
    }
}

void PhaseSwitcher() {
    switch (PhaseState) {
        case PHASE_0:  // Phase 0 - Idle stuff

            LED::BlinkRandomLEDs();
            // LED::Attract();

            if (digitalRead(ROS_START) == LOW) {
                PhaseState = PHASE_1;
                ROS_Start_Activated = true;
                gameCounterTime = millis();
                Serial.println("GAME_STARTED");
                // gameCounterSeconds += 1;
            }
            break;

        case PHASE_1:
            if ((!gameHasStarted) && ROS_Start_Activated && (millis() >= gameCounterTime + 15000)) {
                // This is if the game is started, but then nothing happens anymore, e.g. if you walk away!
                PhaseState = PHASE_0;
                Serial.println("GAME_ENDED");
                ROS_Start_Activated = false;
                Score = 0;
                Multiplier = 1.05;
            }

            CheckAllTargets();
            LED::LightLEDs();

            if (Score != ScoreSent) {
                char buf[50];
                sprintf(buf, "%lu", Score);
                Serial.println(buf);
                ScoreSent = Score;
            }

            // Serial.println(Score);

            return;
            // Phase 1 - Game is runnning
            break;
    }
}

void loop() {
    PhaseSwitcher();
}
