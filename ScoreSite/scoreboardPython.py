#!/usr/bin/python
import serial
import syslog
import time
import array

#The following line is to specify where serial is connected to the Raspberry Pi
port = '/dev/ttyAMC0'

arduinoCommunication = serial.Serial(port,9600,timeout=5) #will be the serial communication

while True:
    # Start the Loop and set the score value to 0 when the game starts
    acomString = str(arduinoCommunication)
    if acomString == "GAME_STARTED":
        acomString = "0"
        with open('scoreSite.html', 'r') as file:
            # read the html file and put it into the data variable
            data = file.readlines()

        # now change Line which displays the current score, note that you have to add a newline
        data[31] = acomString+"\n"

        # and write everything back
        with open('scoreSite.html', 'w') as file:
            file.writelines( data )

    elif acomString == "GAME_ENDED":
        #do stuff and update leaderboard, then exit
        with open('scoreSite.html', 'r') as file:
            # read a list of lines into data
            data = file.readlines()

# now we'll get our values from the scoreboard ranking 1st-5th and start comparing

        data1st = data[15]
        data1stCompare = long(data1st)

        data2nd = data[18]
        data2ndCompare = long(data2nd)

        data3rd = data[21]
        data3rdCompare = long(data3rd)

        data4th = data[24]
        data4thCompare = long(data4th)

        data5th = data[27]
        data5thCompare = long(data5th)

        finalScore = long(previousScoreValue)

#let's compare our score to the leaderboard with the help of array sorting

        list_object = [data1stCompare, data2ndCompare, data3rdCompare, data4thCompare, data5thCompare, finalScore]
        sorted_list = sorted(list_object)

# now we have all the sorted values let's cast them to strings and write the new Leaderboard to the file
        new1st = string(sorted_list[5])
        new2nd = string(sorted_list[4])
        new3rd = string(sorted_list[3])
        new4th = string(sorted_list[2])
        new5th = string(sorted_list[1])

# arrayposition [0] is omitted as it's the lowest value of all six numbers

        data[15] = new1st+"\n"
        data[18] = new2nd+"\n"
        data[21] = new3rd+"\n"
        data[24] = new4th+"\n"
        data[27] = new5th+"\n"

        with open('scoreSite.html', 'w') as file:
            file.writelines( data )

    else:
        with open('scoreSite.html', 'r') as file:
            # read a list of lines into data
            data = file.readlines()

        # now change the the current score to reflect the points while the game is running
        data[31] = acomString+"\n"

        # and write everything back
        with open('scoreSite.html', 'w') as file:
            file.writelines( data )

        previousScoreValue = acomString
