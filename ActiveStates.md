# State 0 - Idle Phase
Game is not running -> no ball is currently in the game
- LEDs are in Idle State
  - LEDs are having a blinking party

Kickback at the bottom is active and reacts to button presses, if any should occur.

**If Switch is pressed, the State changes to 1**

# State 1 - Game has started

**After 30 Seconds of no inputs, game goes back to State 0 - Idle**

Flippers get activated


LEDs in front of Drop and Standing Targets (total: 10 LEDs) are lit.
Otherwise, no LEDs are on.


## Grouped Targets
If all Targets of a certain group were hit, all the respective LEDs turn off and bonus points are granted.

- 4 Standing Targets
- 2 Spinners
- 4 Rollover Switches (The one on the very left is not part of this group)
- Kickback & Scoop Weldment Assembly
- Each 3 Bank Drop Target is its own group

## Scoring

| Part                                   | Score                    | Bonus Score |
| -------------------------------------- | ------------------------ | ----------- |
| Standing Targets                       | 50                       | 200         |
| Spinners                               | 40                       | 220         |
| 3 Bank Drop Target (/Target)           | 30                       | 110         |
| Rollover Switches                      | 100                      | 600         |
| Kickback & Scoop Weldment Assembly     | 50                       | 200         |
| Pop Bumper                             | 5                        | -           |
| Slingshots                             | 5                        | -           |
| "Roundabout"                           | Your Score \*= 1.1       | -           |
| "Roundabout" after all groups were hit | Last Multiplicator + 0.1 | -           |


**If button at bottom is hit, game goes to State 0 - Idle**



# _Maybe State 2_
